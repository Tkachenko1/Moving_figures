package com.vminakov.movingballs.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {
    private double radius;

    private Circle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_CIRCLE, cX, cY, lineWidth, color);
    }

    public Circle(double cX, double cY, double lineWidth, Color color, double radius) {
        this(cX, cY, lineWidth, color);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(LineWidth);
        gc.strokeOval(cX - radius, cY - radius, radius * 2, radius * 2);
    }
}
