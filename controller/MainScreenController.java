package com.vminakov.movingballs.controller;

import com.vminakov.movingballs.figures.Circle;
import com.vminakov.movingballs.figures.Figure;
import com.vminakov.movingballs.figures.Rectangle;
import com.vminakov.movingballs.figures.Triangle;
import com.vminakov.movingballs.interfaces.WorldCallbackListener;
import com.vminakov.movingballs.physics.Body;
import com.vminakov.movingballs.physics.PhysicsWorld;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable, WorldCallbackListener {
    @FXML
    private Canvas canvas;
    private List<Figure> figures;
    private Random rnd;
    private PhysicsWorld world;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, this::processClick);
        figures = new ArrayList<>();
        rnd = new Random(System.currentTimeMillis());
        world = new PhysicsWorld("world",1024,600);
        world.registerCallbackListener(this);
        world.start();
    }

    private void processClick(MouseEvent event) {
        System.out.println("Mouse click detected!");
        Figure f = null;
        double lineWidth = rnd.nextInt(5);
        double r = rnd.nextDouble();
        double g = rnd.nextDouble();
        double b = rnd.nextDouble();
        Color randomColor = new Color(r, g, b, 1);
        switch (rnd.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                double radius = rnd.nextInt(51);
                //System.out.println("Круг {координаты: X=радиус=" +radius+", ");
                f = new Circle(event.getX(), event.getY(), lineWidth == 0 ? 1 : lineWidth, randomColor,
                        radius < 10 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                //System.out.println("Прямоугольник");
                double width = rnd.nextInt(101);
                double height = rnd.nextInt(101);
                f = new Rectangle(event.getX(), event.getY(), lineWidth == 0 ? 1 : lineWidth, randomColor,
                        width < 10 ? 10 : width, height < 10 ? 10 : height);
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                //System.out.println("Треугольник");*/
                double base = rnd.nextInt(101);
                f = new Triangle(event.getX(), event.getY(), lineWidth == 0 ? 1 : lineWidth, randomColor,
                        base < 10 ? 10 : base);
                break;
            default:
                System.out.println("unknown figure type.");
        }
        figures.add(f);
        Body body = world.createBody();
        body.setFigure(f);
        body.setVx(0.35);// скорость по х
        body.setVy(0.25);// cкорость по у
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, 1024, 600);
        figures.forEach((f) -> {
            if (f != null)
                f.draw(canvas.getGraphicsContext2D());
        });
    }

    @Override
    public void callback(){
        Platform.runLater(this::repaint);
    }
}
