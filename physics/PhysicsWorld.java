package com.vminakov.movingballs.physics;


import com.vminakov.movingballs.figures.Circle;
import com.vminakov.movingballs.figures.Figure;
import com.vminakov.movingballs.figures.Rectangle;
import com.vminakov.movingballs.figures.Triangle;
import com.vminakov.movingballs.interfaces.WorldCallbackListener;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class PhysicsWorld extends Thread{
    private double worldWidth;
    private double worldHeight;
    private boolean isRunning;
    private long lastUpdateTime;
    private String type;



    private List<Body> bodies;
    private WorldCallbackListener listener;

    public PhysicsWorld(String name, double worldWidth, double worldHeight) {
        super(name);
        this.worldWidth = worldWidth;
        this.worldHeight = worldHeight;
        bodies = new LinkedList<>();
    }

    public void registerCallbackListener(WorldCallbackListener listener){
        this.listener = listener;
    }

    public Body createBody(){
        Body body = new Body();
        bodies.add(body);
        return body;
    }

    private void checkOuts(){
        bodies.forEach(body -> {

                    if (body.getFigure().getcX() >= worldWidth ||
                    body.getFigure().getcX() <= 0) {
                    body.setVx(body.getVx() * -1);
                    }
                    if (body.getFigure().getcY() >= worldHeight ||
                    body.getFigure().getcY() <= 0) {
                    body.setVy(body.getVy() * -1);
                    }
        });
    }


    @Override
    public void run() {
        isRunning = true;
        lastUpdateTime = System.currentTimeMillis();
        while (isRunning){
            checkOuts();
            bodies.forEach(body -> {
                body.move(System.currentTimeMillis() - lastUpdateTime);
            });
            if (listener != null)
                listener.callback();
            lastUpdateTime = System.currentTimeMillis();
            try {
                Thread.sleep(40L);
            } catch (InterruptedException e) {
                e.printStackTrace();
                isRunning = false;
            }
        }
        bodies.clear();
    }
}


