package com.vminakov.movingballs.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Triangle extends Figure {

    private double base;

    private Triangle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRIANGLE, cX, cY, lineWidth, color);
    }

    public Triangle(double cX, double cY, double lineWidth, Color color, double base) {
        this(cX, cY, lineWidth, color);
        this.base = base;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setStroke(color);
        gc.setLineWidth(LineWidth);
        gc.strokePolygon(new double[]{
                cX,
                cX + base/2,
                cX - base/2
        },new double[]{
                cY - base/2,
                cY + base/2,
                cY + base/2
        }, 3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.base, base) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(base);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Треугольник {");
        sb.append("координата X=").append(cX);
        sb.append(", координата Y=").append(cY);
        sb.append(", длина основания треугольника=").append(base);
        sb.append(", толщина отрисовки линии круга=").append(LineWidth);
        sb.append(", цвет=").append(color);
        sb.append(", площадь треугольника=").append(((base*base)*Math.sqrt(3))/4);
        sb.append('}');
        return sb.toString();
    }
    /*@Override
    public void printInfo () { System.out.println(toString());}

    @Override
    public double getSquare() {
        double square = ((base*base)*Math.sqrt(3))/4;
        return square;
    }*/
}
