package com.vminakov.movingballs.interfaces;

public interface WorldCallbackListener {

    void callback();
}
