package com.vminakov.movingballs.figures;

import com.vminakov.movingballs.interfaces.Drawable;
import javafx.scene.paint.Color;

public abstract class Figure implements Drawable {
    public static final int FIGURE_TYPE_CIRCLE     = 0;
    public static final int FIGURE_TYPE_RECTANGLE  = 1;
    public static final int FIGURE_TYPE_TRIANGLE   = 2;

    //public static final int FIGURE_TYPE_RECT    = 0;
    //public static final int FIGURE_TYPE_CIRCLE  = 1;


    private int type;

    protected double cX;
    protected double cY;
    protected double LineWidth;
    protected Color color;

    public Figure(int type, double cX, double cY, double lineWidth, Color color) {
        this.type = type;
        this.cX = cX;
        this.cY = cY;
        LineWidth = lineWidth;
        this.color = color;
    }

    public int getType() {return type;}

    public double getcX() {return cX;}

    public void setcX(double cX) {this.cX = cX;}

    public double getcY() {return cY;}

    public void setcY(double cY) {this.cY = cY;}

    public double getLineWidth() {return LineWidth;}

    public void setLineWidth(double lineWidth) {LineWidth = lineWidth;}

    public Color getColor() {return color;}

    public void setColor(Color color) {this.color = color;}

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Figure{");
        sb.append("type=").append(type);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", LineWidth=").append(LineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }
}